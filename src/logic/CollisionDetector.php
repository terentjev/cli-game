<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace logic;


use visual\component\VisualComponent;

/**
 * Class CollisionDetector
 * @package logic
 */
class CollisionDetector {
    private VisualComponent $component;

    private array $coords;

    public function __construct(VisualComponent $component) {
        $this->component = $component;

        $this->coords = array();
    }

    public function findCollision(): void {
        foreach ($this->component->getEntities() as $entity) {
            $this->coords[$entity->getId()] = $entity->getCoords()->__toString();
        }

        foreach ($this->getDuplicates() as $entityId => $coords) {
            $this->component->getEntityById($entityId)->onCollisionEnter();
        }
    }

    private function getDuplicates(): array {
        return array_unique(array_diff_assoc($this->coords, array_unique($this->coords)));
    }
}