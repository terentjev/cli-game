<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual;


/**
 * Class Coords
 * @package visual
 */
class Coords {
    private int $xLeftTop;
    private int $yLeftTop;
    private int $xRightBottom;
    private int $yRightBottom;

    public function __construct(int $xLeftTop, int $yLeftTop, int $xRightBottom, int $yRightBottom) {
        $this->xLeftTop = $xLeftTop;
        $this->yLeftTop = $yLeftTop;
        $this->xRightBottom = $xRightBottom;
        $this->yRightBottom = $yRightBottom;
    }

    /**
     * @return int
     */
    public function getXLeftTop(): int {
        return $this->xLeftTop;
    }

    /**
     * @return int
     */
    public function getYLeftTop(): int {
        return $this->yLeftTop;
    }

    /**
     * @return int
     */
    public function getXRightBottom(): int {
        return $this->xRightBottom;
    }

    /**
     * @return int
     */
    public function getYRightBottom(): int {
        return $this->yRightBottom;
    }
}