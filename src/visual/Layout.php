<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual;


use visual\component\Component;
use visual\component\VisualComponent;
use visual\io\Output;

/**
 * Class Layout
 * @package visual
 */
class Layout {
    private array $components;

    public function __construct() {
        $this->components = array();
    }

    public function addComponent(Component $component): void {
        $this->components[$component->getName()] = $component;
    }

    public function getComponent(string $name): VisualComponent {
        return $this->components[$name];
    }

    public function getComponents(): array {
        return $this->components;
    }

    public function render(): void {
        Output::clear();

        /** @var $component Component */
        foreach ($this->components as $component) {
            $component->render();
        }

        Output::emptyLine();
    }
}
