<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\io;


/**
 * Class Output
 * @package visual\io
 */
class Output {
    public static function render(?string $any, string $color = OutputColor::DEFAULT): void {
        echo $color . $any;
    }

    public static function setPos(int $x, int $y): void {
        echo "\033[" . $x . ";" . $y . "H";
    }

    public static function up(int $n): void {
        echo "\033[" . $n . "A";
    }

    public static function down(int $n): void {
        echo "\033[" . $n . "B";
    }

    public static function forward(int $n): void {
        echo "\033[" . $n . "C";
    }

    public static function backward(int $n): void {
        echo "\033[" . $n . "D";
    }

    public static function emptyLine(): void {
        echo PHP_EOL;
    }

    public static function clear(): void {
        echo "\033[2J";
    }
}