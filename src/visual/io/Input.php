<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\io;


/**
 * Class Input
 * @package visual\io
 */
class Input {
    public function __construct() {
        $callback = function () { echo PHP_EOL; };
        readline_callback_handler_install('', $callback);
    }

    public function __destruct() {
        readline_callback_handler_remove();
    }

    public static function getUserInput(): string {
        return trim(fgetc(STDIN));
    }
}