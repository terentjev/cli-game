<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity;


/**
 * Class AbstractEntity
 * @package visual\entity
 */
abstract class AbstractEntity implements Entity {
    protected string $id;

    public function __construct() {
        $this->id = uniqid();
    }

    abstract public function render(): void;

    abstract public function onCollisionEnter(): void;

    public function getId(): string {
        return $this->id;
    }
}
