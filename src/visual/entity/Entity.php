<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity;


interface Entity {
    public function render(): void;
}