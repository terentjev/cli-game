<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity\info;


use visual\entity\AbstractEntity;
use visual\io\Output;

/**
 * Class InfoEntity
 * @package visual\entity\info
 */
class InfoEntity extends AbstractEntity {
    private string $content;

    public function __construct(string $content) {
        parent::__construct();

        $this->content = $content;
    }

    public function onCollisionEnter(): void {
        return;
    }

    public function render(): void {
        Output::render($this->content);
        Output::emptyLine();
    }
}
