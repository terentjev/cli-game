<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity\playable;


use visual\io\OutputColor;

/**
 * Class Hiro
 * @package visual\entity\playable
 */
class Hiro extends PlayableEntity {
    protected string $characterSymbol = '@';
    public int $killEnemyCount = 0;

    public function __construct() {
        parent::__construct();
        $this->id = 'hiro';
        $this->coords = new Coords();

        $this->coords->setX(mt_rand(4, 14));
        $this->coords->setY(mt_rand(2, 5));
    }

    public function action(string $input = null): void {
        switch ($input) {
            case 'w':
                $this->coords->setX($this->coords->getX() - 1);
                break;
            case 'a':
                $this->coords->setY($this->coords->getY() - 1);
                break;
            case 's':
                $this->coords->setX($this->coords->getX() + 1);
                break;
            case 'd':
                $this->coords->setY($this->coords->getY() + 1);
                break;
            default:
                break;
        }
    }

    public function onCollisionEnter(): void {
        $this->characterSymbol = OutputColor::BLUE . $this->characterSymbol;
        $this->killEnemyCount += 1;
    }
}