<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity\playable;


use visual\io\OutputColor;

/**
 * Class Rabotyaga
 * @package visual\entity\playable
 */
class Rabotyaga extends PlayableEntity {
    protected string $characterSymbol = '#';

    private int $gold;
    private int $braveryLevel;

    public function __construct() {
        parent::__construct();

        $this->coords = new Coords();
        $this->coords->setX(mt_rand(2, 10));
        $this->coords->setY(mt_rand(2, 4));

        $this->gold = mt_rand(1, 100);
        $this->braveryLevel = mt_rand(0, 5);
    }

    public function action(string $input = null): void {
        $heroCoords = new Coords();
        $heroCoords->setX(mt_rand(2, 10));
        $heroCoords->setY(mt_rand(2, 5));
        if ($this->isDangerZone($heroCoords)) {
            $this->runAway($heroCoords);
        } else {
            $this->walk();
        }
    }

    public function isDangerZone(Coords $heroCoords): bool {
        $xDistance = abs($this->coords->getX() - $heroCoords->getX());
        $yDistance = abs($this->coords->getY() - $heroCoords->getY());

        return ($this->braveryLevel < $xDistance && $this->braveryLevel < $yDistance);
    }

    public function walk(): void {
        $xPos = $this->coords->getX();
        $yPos = $this->coords->getY();

        $step = mt_rand(-1, 1);
        if ($step === 0) {
            return;
        }

        $nextYPos = mt_rand($yPos, $yPos + $step);
        if ($nextYPos != $yPos && $nextYPos >= 0) {
            $this->coords->setY($nextYPos);
            return;
        }

        $nextXPos = mt_rand($xPos, $xPos + $step);
        if ($nextXPos != $xPos && $nextXPos >= 0) {
            $this->coords->setX($nextXPos);
            return;
        }
    }

    public function runAway(Coords $heroCoords): void {
        $xStep = $this->coords->getX() > $heroCoords->getX() ? 1 : -1;
        $yStep = $this->coords->getY() > $heroCoords->getY() ? 1 : -1;

        $this->coords->setX($this->coords->getX() + $xStep);
        $this->coords->setY($this->coords->getY() + $yStep);
    }

    public function onCollisionEnter(): void {
        $this->characterSymbol = OutputColor::RED . $this->characterSymbol;
    }
}