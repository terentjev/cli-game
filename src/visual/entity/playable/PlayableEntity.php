<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity\playable;


use visual\entity\AbstractEntity;
use visual\io\Output;

/**
 * Class PlayableEntity
 * @package visual\entity\playable
 */
abstract class PlayableEntity extends AbstractEntity {
    protected string $characterSymbol;

    protected Coords $coords;

    public function render(): void {
        Output::setPos($this->coords->getX(), $this->coords->getY());
        Output::render($this->characterSymbol);
    }

    public function getCoords(): Coords {
        return $this->coords;
    }

    public function setCoords(Coords $coords): void {
        $this->coords = $coords;
    }

    abstract public function action(string $input = null): void;

    abstract public function onCollisionEnter(): void;
}
