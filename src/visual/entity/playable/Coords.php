<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\entity\playable;


/**
 * Class Coords
 * @package visual\entity\playable
 */
class Coords {
    private int $x;
    private int $y;

    /**
     * @return int
     */
    public function getX(): int {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX(int $x): void {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY(): int {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY(int $y): void {
        $this->y = $y;
    }

    public function __toString() {
        return $this->x . ';' . $this->y;
    }
}