<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\component;


use visual\entity\AbstractEntity;
use visual\entity\Entity;
use visual\entity\playable\Coords;
use visual\entity\playable\PlayableEntity;
use visual\io\Output;

/**
 * Class VisualComponent
 * @package visual\component
 */
abstract class VisualComponent implements Component {
    protected string $emptyChar;

    protected array $entities = array();

    public function addEntity(AbstractEntity $entity): void {
        $this->entities[$entity->getId()] = $entity;
    }

    public function getEntities(): array {
        return $this->entities;
    }

    public function getEntityById(string $id): AbstractEntity {
        return $this->entities[$id];
    }

    public function render(): void {
        $coords = $this->getCoords();

        Output::setPos($coords->getYLeftTop(), $coords->getXLeftTop());
        foreach ($this->entities as $entity) {
            $entity->render();
        }
    }

    final public function processEntities(): void {
        foreach ($this->entities as $entity) {
            if (!$entity instanceof PlayableEntity) {
                continue;
            }

            $entity->action();
        }
    }
}
