<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\component;


use visual\Coords;

interface Component {
    public function getName(): string;

    public function getCoords(): Coords;
}