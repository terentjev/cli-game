<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\component;


use visual\Coords;
use visual\entity\AbstractEntity;

/**
 * Class BattleLog
 * @package visual\component
 */
class BattleLog extends VisualComponent {
    protected string $emptyChar = ' ';

    public function getName(): string {
        return 'battlelog';
    }

    public function getCoords(): Coords {
        return new Coords(0, 12, 60, 15);
    }

    public function addEntity(AbstractEntity $entity): void {
        if (count($this->entities) > 3) {
            array_shift($this->entities);
        }

        parent::addEntity($entity);
    }
}