<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\component;


use visual\Coords;

/**
 * Class Menu
 * @package visual\component
 */
class Menu extends VisualComponent {
    protected string $emptyChar = ' ';

    public function getName(): string {
        return 'menu';
    }

    public function getCoords(): Coords {
        return new Coords(54, 0, 60, 10);
    }
}