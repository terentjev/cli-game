<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

namespace visual\component;


use visual\Coords;
use visual\entity\Entity;
use visual\entity\playable\PlayableEntity;
use visual\io\Output;

/**
 * Class Map
 * @package visual\component
 */
class Map extends VisualComponent {
    protected string $emptyChar = '';

    public function getName(): string {
        return 'map';
    }

    public function getCoords(): Coords {
        return new Coords(0,0, 50, 10);
    }

    public function render(): void {
            $coords = $this->getCoords();

            Output::setPos($coords->getYLeftTop(), $coords->getXLeftTop());
            foreach ($this->entities as $entity) {
                /** @var $entity Entity */
                if ($entity instanceof PlayableEntity) {
                    $entityCoords = $this->calcEntityCoords($entity);
                    $entity->setCoords($entityCoords);
                }

                $entity->render();
            }
        }

    protected function calcEntityCoords(PlayableEntity $entity): \visual\entity\playable\Coords {
        $coords = $this->getCoords();

        $entityCoords = $entity->getCoords();

        if ($entityCoords->getX() <= $coords->getYLeftTop()) {
            $entityCoords->setX($coords->getYLeftTop());
        }

        if ($entityCoords->getX() > $coords->getYRightBottom()) {
            $entityCoords->setX($coords->getYRightBottom());
        }

        if ($entityCoords->getY() <= $coords->getXLeftTop()) {
            $entityCoords->setY($coords->getXLeftTop());
        }

        if ($entityCoords->getY() > $coords->getXRightBottom()) {
            $entityCoords->setY($coords->getXRightBottom());
        }

        return $entityCoords;
    }
}