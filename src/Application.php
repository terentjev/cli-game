<?php

/**
 * @author Ivan Abashin <abashin@intervolga.ru>
 */

use logic\CollisionDetector;
use visual\component;
use visual\entity\playable\Hiro;
use visual\io\Input;
use visual\Layout;

/**
 * Class Application
 */
class Application {
    private Input $input;
    private Layout $layout;
    private Hiro $hiro;

    private CollisionDetector $collisionDetector;

    public function __construct() {
        $this->input = new Input();
        $this->layout = new Layout();
        $this->hiro = new Hiro();

        $this->layout->addComponent(new component\Map());
        $this->layout->addComponent(new component\Menu());
        $this->layout->addComponent(new component\BattleLog());

        $this->collisionDetector = new CollisionDetector($this->layout->getComponent('map'));
    }

    public function run(): void {
        $this->layout->getComponent('battlelog')->addEntity(new \visual\entity\info\InfoEntity('Igra+'));
        $this->layout->getComponent('map')->addEntity(new \visual\entity\playable\Rabotyaga());
        $this->layout->getComponent('map')->addEntity(new \visual\entity\playable\Rabotyaga());
        $this->layout->getComponent('map')->addEntity(new \visual\entity\playable\Rabotyaga());
        $this->layout->getComponent('map')->addEntity($this->hiro);

        $components = $this->layout->getComponents();
        while (true) {
            foreach ($components as $component) {
                $component->processEntities();
            }

            $this->collisionDetector->findCollision();

            $this->layout->render(); // Map, IO
            $this->hiro->action(Input::getUserInput());

            $this->collisionDetector->findCollision();

            $this->layout->getComponent('battlelog')->addEntity(new \visual\entity\info\InfoEntity(
                'Ograbil yje ' . $this->layout->getComponent('map')->getEntityById('hiro')->killEnemyCount . ' korovanov!'
            ));
        }
    }
}
